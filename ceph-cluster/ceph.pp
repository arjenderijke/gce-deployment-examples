$admin_key = 'AQCTg71RsNIHORAAW+O6FCMZWBjmVfMIPk3MhQ=='
$mon_key = 'AQDesGZSsC7KJBAAw+W/Z4eGSQGAIbxWjxjvfw=='
$bootstrap_osd_key = 'AQABsWZSgEDmJhAAkAGSOOAJwrMHrM5Pz5On1A=='
$fsid = '066F558C-6789-4A93-AAF1-5AF1BA01A3AD'

node /cephmon*/ {
  class { 'ceph::repo': }
  class { 'ceph':
    fsid                => $fsid,
    mon_initial_members => 'cephmon',
    mon_host            => '10.132.0.2',
  }
  ceph::mon { $::hostname:
    key => $mon_key,
  }
  Ceph::Key {
    inject         => true,
    inject_as_id   => 'mon.',
    inject_keyring => "/var/lib/ceph/mon/ceph-${::hostname}/keyring",
  }
  ceph::key { 'client.admin':
    secret  => $admin_key,
    cap_mon => 'allow *',
    cap_osd => 'allow *',
    cap_mds => 'allow',
  }
  ceph::key { 'client.bootstrap-osd':
    secret  => $bootstrap_osd_key,
    cap_mon => 'allow profile bootstrap-osd',
  }
}

node /cephosd*/ {
  class { 'ceph::repo': }
  class { 'ceph':
    fsid                => $fsid,
    mon_initial_members => 'cephmon',
    mon_host            => '10.132.0.2',
  }
  ceph::osd {
    '/dev/sdb':
      journal => '/dev/sdb';
  }
  ceph::key {'client.bootstrap-osd':
    keyring_path => '/var/lib/ceph/bootstrap-osd/ceph.keyring',
    secret       => $bootstrap_osd_key,
  }
}

node /puppetceph/ {
  class { 'ceph::repo': }
  class { 'ceph':
    fsid                => $fsid,
    mon_initial_members => 'cephmon',
    mon_host            => '10.132.0.2',
  }
  ceph::key { 'client.admin':
    secret => $admin_key
  }
}

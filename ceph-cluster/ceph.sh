sudo rpm -Uvh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
sudo yum -y install puppet-agent git emacs
git clone https://github.com/openstack/puppet-ceph
cd puppet-ceph/
/opt/puppetlabs/puppet/bin/puppet module build
sudo /opt/puppetlabs/puppet/bin/puppet module install /home/arjenderijke/puppet-ceph/pkg/openstack-ceph-2.2.1.tar.gz
emacs ~/ceph.pp
sudo /opt/puppetlabs/puppet/bin/puppet apply /home/arjenderijke/ceph.pp

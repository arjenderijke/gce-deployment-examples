# gce deployment examples

## Examples

### puppet ceph
- Start a ceph cluster using the puppet-ceph module

## Urls
- https://cloud.google.com/solutions/high-throughput-computing-htcondor
- https://cloudplatform.googleblog.com/2016/11/what-is-Google-Cloud-Deployment-Manager-and-how-to-use-it.html
- https://github.com/openstack/puppet-ceph/blob/master/USECASES.md
